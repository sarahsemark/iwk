<?php

/**
 * This sample app is provided to kickstart your experience using Facebook's
 * resources for developers.  This sample app provides examples of several
 * key concepts, including authentication, the Graph API, and FQL (Facebook
 * Query Language). Please visit the docs at 'developers.facebook.com/docs'
 * to learn more about the resources available to you
 */

// Provides access to app specific values such as your app id and app secret.
// Defined in 'AppInfo.php'
require_once('AppInfo.php');

// Enforce https on production
if (substr(AppInfo::getUrl(), 0, 8) != 'https://' && $_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
  header('Location: https://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
  exit();
}

// This provides access to helper functions defined in 'utils.php'
require_once('utils.php');


/*****************************************************************************
 *
 * The content below provides examples of how to fetch Facebook data using the
 * Graph API and FQL.  It uses the helper functions defined in 'utils.php' to
 * do so.  You should change this section so that it prepares all of the
 * information that you want to display to the user.
 *
 ****************************************************************************/

require_once('sdk/src/facebook.php');

$facebook = new Facebook(array(
  'appId'  => AppInfo::appID(),
  'secret' => AppInfo::appSecret(),
));

// Fetch the basic info of the app that they are using
$app_info = $facebook->api('/'. AppInfo::appID());

$app_name = idx($app_info, 'name', '');

?>
<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes" />

    <title><?php echo he($app_name); ?></title>

	<!-- CSS
  ================================================== -->
	<link rel="stylesheet" href="skeleton/stylesheets/base.css">
	<link rel="stylesheet" href="skeleton/stylesheets/skeleton.css">
	<link rel="stylesheet" href="skeleton/stylesheets/layout.css">
	
	<link rel="stylesheet" href="fonts/digital/stylesheet.css">
	<link rel="stylesheet" href="fonts/frutiger/stylesheet.css">
	<link rel="stylesheet" href="fonts/archer/stylesheet.css">
	<link rel="stylesheet" href="fonts/museo-slab-light/stylesheet.css">	


	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

    <!-- These are Open Graph tags.  They add meta data to your  -->
    <!-- site that facebook uses when your content is shared     -->
    <!-- over facebook.  You should fill these tags in with      -->
    <!-- your data.  To learn more about Open Graph, visit       -->
    <!-- 'https://developers.facebook.com/docs/opengraph/'       -->
    <meta prefix="fb: http://ogp.me/ns/fb#" property="fb:app_id" content="<?php echo AppInfo::appID(); ?>" />
    <meta property="og:title" content="<?php echo he($app_name); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo AppInfo::getUrl(); ?>" />
    <meta property="og:image" content="<?php echo AppInfo::getUrl('/logo.png'); ?>" />
    <meta property="og:site_name" content="<?php echo he($app_name); ?>" />
    <meta property="og:description" content="My first app" />

    <script type="text/javascript" src="/javascript/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/javascript/app.min.js"></script>

    <!--[if IE]>
      <script type="text/javascript">
        var tags = ['header', 'section'];
        while(tags.length)
          document.createElement(tags.pop());
      </script>
    <![endif]-->
  </head>
  <body id="breastclinic">
    <div id="fb-root"></div>
    <script type="text/javascript">
    
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo AppInfo::appID(); ?>', // App ID
          channelUrl : 'https://<?php echo $_SERVER["HTTP_HOST"]; ?>/channel.html', // Channel File
          status     : true, // check login status
          cookie     : true, // enable cookies to allow the server to access the session
          xfbml      : true // parse XFBML
        });

        FB.Canvas.setAutoGrow();
      };

      // Load the SDK Asynchronously
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/all.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>

	<!-- Primary Page Layout
	================================================== -->

	<!-- Delete everything in this .container and get started on your own site! -->

	<div id="facebook-plugins" class="container">
		<h3>Like</h3>
		<div class="fb-like" data-href="https://apps.facebook.com/twenty_four_hours/" data-send="false" data-width="450" data-show-faces="false" data-font="lucida grande"></div>
		<br/><hr/><br/>

		<h3>Like + Send</h3>
		<div class="fb-like" data-href="https://apps.facebook.com/twenty_four_hours/" data-send="true" data-width="450" data-show-faces="false" data-font="lucida grande"></div>
		<br/><hr/><br/>

		<h3>Recommend</h3>
		<div class="fb-like" data-href="https://apps.facebook.com/twenty_four_hours/" data-send="false" data-width="450" data-show-faces="false" data-action="recommend" data-font="lucida grande"></div>
		<br/><hr/><br/>

		<h3>Recommend + Send</h3>
		<div class="fb-like" data-href="https://apps.facebook.com/twenty_four_hours/" data-send="true" data-width="450" data-show-faces="false" data-action="recommend" data-font="lucida grande"></div>
		<br/><hr/><br/>

		<h3>Post to wall</h3>
		<a href="#post-to-wall" class="post-to-wall" data-href="https://apps.facebook.com/twenty_four_hours/">Post to wall</a>
		<br/><hr/><br/>

    <h3>Post to wall + custom data</h3>
    <a href="#post-to-wall" class="post-to-wall" data-href="https://apps.facebook.com/twenty_four_hours/" data-picture="https://apps.facebook.com/twenty_four_hours/images/breast-health/_DSC6140_web.jpg" data-name="Custom name" data-caption="Custom caption" data-description="Custom description">Post to wall + custom data</a>
    <br/><hr/><br/>

		<h3>Send to friends</h3>
		<a href="#send-to-friends" class="send-to-friends" data-href="https://apps.facebook.com/twenty_four_hours/">Send to friends</a>
		<br/><hr/><br/>

		<h3>Comments</h3>
		<div class="fb-comments" data-href="https://apps.facebook.com/twenty_four_hours/" data-num-posts="4" data-width="400"></div>
	</div>
  </body>
</html>