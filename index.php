<?php

// Provides access to app specific values such as your app id and app secret.
// Defined in 'AppInfo.php'
require_once('AppInfo.php');

// Enforce https on production
if (substr(AppInfo::getUrl(), 0, 8) != 'https://' && $_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
  header('Location: https://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
  exit();
}

// This provides access to helper functions defined in 'utils.php'
require_once('utils.php');

/*****************************************************************************
 *
 * The content below provides examples of how to fetch Facebook data using the
 * Graph API and FQL.  It uses the helper functions defined in 'utils.php' to
 * do so.  You should change this section so that it prepares all of the
 * information that you want to display to the user.
 *
 ****************************************************************************/

require_once('sdk/src/facebook.php');

$facebook = new Facebook(array(
  'appId'  => AppInfo::appID(),
  'secret' => AppInfo::appSecret(),
));

// Fetch the basic info of the app that they are using
$app_info = $facebook->api('/'. AppInfo::appID());

$app_name = idx($app_info, 'name', '');

// OLD URL Router with ?p=
// $allowed_files = array(
// 	'breastclinic',
// 	'presidentsletter',
// 	'nicu',
// 	'picu',
// 	'pediatricrehab',
// 	'emergency',
// 	'mentalhealth',
// 	'communitycare', 
// 	'achievements',
// 	'financials',
// 	'download', 
// 	'letter'
// );

// $page = isset( $_REQUEST['p'] ) ? strtolower( trim( $_REQUEST['p'] ) ) : 'home.php';

// if ( ! $page || empty( $page ) ) {
// 	include_once 'home.php';
// } elseif ( in_array($page, $allowed_files)) {
// 	include_once "{$page}.php";
// } else {
// 	// You should 404 here if possible; they've requested a file you don't allow to be included.
// 	include_once 'home.php';
// 	// include_once '404.php';
// }

// New URL Router with clean urls /letter/
$request_uri = isset( $_SERVER['REQUEST_URI'] ) ? trim( $_SERVER['REQUEST_URI'] ) : false;
$query_string = strpos($request_uri, '?');

if ( $query_string === FALSE )
	list($path, $params) = array($request_uri, NULL);
else
	list($path, $params) = array(substr($request_uri, 0, $query_string), substr($request_uri, $query_string + 1));

$allowed_files = array(
	'breastclinic\/?' => 'breastclinic.php',
	'presidentsletter\/?' => 'presidentsletter.php',
	'nicu\/?' => 'nicu.php',
	'picu\/?' => 'picu.php',
	'pediatricrehab\/?' => 'pediatricrehab.php',
	'emergency\/?' => 'emergency.php',
	'mentalhealth\/?' => 'mentalhealth.php',
	'communitycare\/?' => 'communitycare.php',
	'achievements\/?' => 'achievements.php',
	'financials\/?' => 'financials.php',
	'download\/?' => 'download.php',
	'letter\/?' => 'letter.php',
	'404\/?' => '404.php',
);

$page = false;
foreach ( $allowed_files as $regex => $file ) {
	if ( preg_match( "#{$regex}#im", $path ) ) {
		$page = $file;
		break;
	}
}

// homepage
if ( ! $page && $path == '/' ) {
	$page = 'home.php';
} elseif ( ! $page ) { // 404
	$page = 'home.php';
	// $page = '404.php';
}

include_once $page;

?>
<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes" />

    <title><?php echo he($app_name); ?>: : <?php echo $title; ?></title>

	<!-- CSS
  ================================================== -->
	<link rel="stylesheet" href="/skeleton/stylesheets/base.css">
	<link rel="stylesheet" href="/skeleton/stylesheets/skeleton.css">
	<link rel="stylesheet" href="/skeleton/stylesheets/layout.css">
	
	<link rel="stylesheet" href="/fonts/digital/stylesheet.css">
	<link rel="stylesheet" href="/fonts/frutiger/stylesheet.css">
	<link rel="stylesheet" href="/fonts/museo-slab-light/stylesheet.css">	

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	
	<script src="/lightbox/js/lightbox.js"></script>
	<link href="/lightbox/css/lightbox.css" rel="stylesheet" />
	
    <!-- These are Open Graph tags.  They add meta data to your  -->
    <!-- site that facebook uses when your content is shared     -->
    <!-- over facebook.  You should fill these tags in with      -->
    <!-- your data.  To learn more about Open Graph, visit       -->
    <!-- 'https://developers.facebook.com/docs/opengraph/'       -->

    <?php
    	// Fix og tags
    	$og_url = ( $url == 'home' ) ? '/' : "{$url}/";
    	$og_url = AppInfo::getAppUrl( $og_url );
    	// $og_url = AppInfo::getUrl( $og_url );
    	$og_description = strip_tags( preg_replace( '#\<br\s*?\/?\>#im', ' ', $description ) );
    ?>
    <meta prefix="fb: http://ogp.me/ns/fb#" property="fb:app_id" content="<?php echo AppInfo::appID(); ?>" />
    <meta property="og:title" content="<?php echo he($app_name); ?>: <?php echo $title; ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo $og_url; ?>" />
    <meta property="og:image" content="<?php echo AppInfo::getUrl('/images/logo.png'); ?>" />
    <?php /*<meta property="og:site_name" content="<?php echo he($app_name); ?>" />*/ ?>

    <meta property="og:description" content="<?php echo $og_description; ?>" />

    <script type="text/javascript" src="/javascript/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/javascript/app.min.js"></script>

    <!--[if IE]>
      <script type="text/javascript">
        var tags = ['header', 'section'];
        while(tags.length)
          document.createElement(tags.pop());
      </script>
    <![endif]-->
  </head>
  <body id="<?php echo $url; ?>">
  	
    <div id="fb-root"></div>
    <script type="text/javascript">
    
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo AppInfo::appID(); ?>', // App ID
          channelUrl : 'https://<?php echo $_SERVER["HTTP_HOST"]; ?>/channel.html', // Channel File
          status     : true, // check login status
          cookie     : true, // enable cookies to allow the server to access the session
          xfbml      : true // parse XFBML
        });

        FB.Canvas.setAutoGrow();
      };

      // Load the SDK Asynchronously
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/all.js#xfbml=1&appId=248214445232543";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>

	<!-- Primary Page Layout
	================================================== -->

	<div class="container">
	
		<?php if ($url == "home"): //show homepage ?>
		
			<?php echo $narration; ?>	

		<?php elseif ($url == "financials"): //show financials page ?>
			<div class="twelve columns">
				<h1><?php echo $title; ?></h1>
				<hr />
			</div>
			
			<?php echo $narration; ?>	
			
		<?php else: // show any other pages ?>
		
			<div class="twelve columns">
				<h1><?php echo $title; ?></h1>
				<hr />
			</div>

			<div class="twelve columns factoid">
				<?php echo $description; ?>
			</div>
			
			<div class="eight columns clearfix">

				<img src="images/<?php echo $url; ?>.jpg" alt="<?php echo ($title); ?>">
				
				<?php if ($url == "letter"): ?>
					<img src="images/<?php echo $url; ?>-2.jpg" alt="<?php echo ($title); ?>" width="457" height="457">
				<?php endif;  ?>		

				<?php if (isset($images)):  // only show images if the page uses an image ?>

					<a id="viewmore" href="images/<?php echo $url; ?>/<?php echo $images[0]; ?>" rel="lightbox[slideshow]">View more images</a>

					<div class="extra-images">		
					<?php 		
						// remove first two items from array first
						$images = array_slice($images, 2);
						foreach ($images as $image) {
							echo '<a href="images/'.$url.'/'.$image.'" rel="lightbox[slideshow]">Image</a>';		
						}
					?>
					</div>
			
				<?php endif; // end images control loop ?>		
			
				<?php 
				
				foreach ($quotes as $quote => $source) {
						echo '<blockquote><span class="bqstart">&ldquo;</span>';
						echo $quote;
						echo '<span class="bqend">&rdquo;</span></blockquote><p class="quote_source">';
						echo $source;	
						echo '</p>';
					}
			
				?>

			</div>
			
			<div class="eight columns">
			
				<?php if (isset($time)): ?>
					<div class="four columns offset-by-four time"><?php echo $time; ?></div>
				<?php endif; ?>

				<?php if (isset($lat)): ?>
					<div class="four columns alpha"> &nbsp; </div>
					<div class="four columns omega coords"><p><?php echo $lat; ?> N</p><p><?php echo $long; ?> W</p></div>
				<?php endif; ?>
			
				<div class="narration">
					<?php echo $narration; ?>
				</div>
			</div>
			
		<?php endif; // end specific page loop ?>
		
		<div class="clearfix"></div>


		<div class="sixteen columns">
		<a href="#post-to-wall" class="post-to-wall" data-href="<?php echo $og_url; ?>">Post to wall</a>
		<div class="fb-like" data-href="<?php echo $og_url; ?>" data-send="true" data-width="450" data-show-faces="false" data-font="lucida grande"></div>
		</div>
		
		<div class="sixteen columns comment-area">
			<div class="fb-comments" data-href="<?php echo $og_url; ?>" data-num-posts="10"></div>
		</div>
		
	</div><!-- container -->

	<div id="sidebar" <?php if ($url == "home") { echo 'class="active"'; } ?>>
		<ul>
			<li><a <?php if ($url == "letter") { echo 'class="selected"'; } ?>id="presidents-link" href="/letter">President&rsquo;s Message</a></li>
			<li><a <?php if ($url == "breastclinic") { echo 'class="selected"'; } ?> id="breastclinic-link" href="/breastclinic">Breast Health Clinic</a></li>
			<li><a <?php if ($url == "mentalhealth") { echo 'class="selected"'; } ?> id="mentalhealth-link" href="/mentalhealth">Mental Health Inpatient Services</a></li>
			<li><a <?php if ($url == "communitycare") { echo 'class="selected"'; } ?> id="communitycare-link" href="/communitycare">Community Wellness Centre</a></li>
			<li><a <?php if ($url == "pediatricrehab") { echo 'class="selected"'; } ?> id="pediatric-link" href="/pediatricrehab">Pediatric Rehabilitation Care Team</a></li>
			<li><a <?php if ($url == "nicu") { echo 'class="selected"'; } ?> id="nicu-link" href="/nicu">Neonatal Intensive Care Unit (NICU)</a></li>
			<li><a <?php if ($url == "picu") { echo 'class="selected"'; } ?> id="picu-link" href="/picu">Pediatric Intensive Care Unit (PICU)</a></li>

			<li><a <?php if ($url == "emergency") { echo 'class="selected"'; } ?> id="emergency-link" href="/emergency">Emergency Department</a></li>
			<li><a <?php if ($url == "financials") { echo 'class="selected"'; } ?> id="facts-link" href="/financials">Financials</a></li>
			<li><a <?php if ($url == "achievements") { echo 'class="selected"'; } ?> id="achievements-link" href="/achievements">Board Achievements</a></li>
			<li><a <?php if ($url == "download") { echo 'class="selected"'; } ?> id="pdf-link" href="/download">Download PDFs</a></li>
		</ul>	
	</div>
  </body>
</html>
