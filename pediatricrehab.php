<?php

$url = "pediatricrehab";
$title = "Pediatric Rehabilitation Care Team";
$description = "The rehabilitation service is a compilation of <em>four clinics, eight professions and 10 teams</em>. Services are provided in inpatient units, outpatient areas and clinics, homes, preschools, and schools.";


$time = "1430";

$lat = "44°39'34\"";
$long = "63°35'50\"";



$narration = <<<HEREDOC
<p>Tucked away amongst the mature trees and charming homes of the North End of Halifax is Joseph Howe School. From the outside, this one-level brick building looks as sleepy as schools everywhere do in the middle of summer. But inside, nine children are having the time of their lives. All eligible to start school in September, the laughter and squeals from this boisterous group of four- and five-year-olds is bouncing off the walls. If you closed your eyes, you&rsquo;d never know that they all have serious physical or intellectual disabilities. In gym class they are dancing, shaking rhythm sticks, kicking bouncy balls&hellip; this looks and sounds like nothing more than a fun time.</p>
<p>Called Big School Here I Come, it&rsquo;s actually part of a three-day program to ease the transition for parents and kids alike into grade primary. An offering of the IWK&rsquo;s Rehabilitation Care Team, its goal is to trial various tools, techniques and strategies that have been recommended by health professionals to help these children thrive in elementary school. Immediately following the program, reports are prepared for each child&rsquo;s local school, offering valuable insight into what strategies are functional and practical in the school environment and can be introduced on the first day of school. For the children, these three days allow them to begin to develop skills they never knew they needed. Meeting new friends, responding to fire drills, eating their snacks, listening to teachers, and sitting at a desk are just a few of the things that will come a little easier in September. For parents, there is a session regarding the importance of partnering with the school while at the same time advocating for their child.</p>
HEREDOC;

$images = array('_DSC6294_web.jpg', '_DSC6304_web.jpg', '_DSC6314_web.jpg', '_DSC6320_web.jpg', '_DSC6325_web.jpg', '_DSC6362_web.jpg', '_DSC6297_web.jpg', '_DSC6306_web.jpg', '_DSC6316_web.jpg', '_DSC6322_web.jpg', '_DSC6335_web.jpg');

$quotes = array(
		'For children, life skills are the skills they need to participate well in all aspects of life. The activities that children participate in during Big School Here I Come might be putting their hand up, responding to a fire alarm, having to speak to a Principal, and asking for what they need without having their parent beside them. They quickly discover all sorts of things about themselves, like how to approach another child and say &lsquo;Hi,&rsquo; how to sit and listen to the teacher and that they can be self-sufficient with some things, perhaps for the first time. Rehabilitation teaches children the life skills they need in physical, social and emotional aspects of daily life. The goal is to have them participate in and enjoy the best quality of life they can.' 
	=> 'Kim Clarke<br>Administration Assistant<br>Pediatric Rehabilitation Service, IWK'
);
