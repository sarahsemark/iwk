<?php

$url = "home";
$title = "24 Hours";
$description = "A behind-the-scenes look into life At the IWK Health Centre<br>2011/2012 Report to the Community";


//$time = "1405";

//$lat = "44°38'26";
//$long = "63°35'05";

$narration = <<<HEREDOC

	<div class="eight columns offset-by-seven factoid">
	
	<img src="/images/home.png" alt="home" width="567" height="610">
	
	<p>Over two days in August, a writer, photographer and graphic designer went behind the scenes at the IWK in an <em>unprecedented way</em>. Over the course of their 24-hour journey, <em>seven different units and functions</em> of the Health Centre were explored.</p>	
	
	</div>
	
HEREDOC;
