/**
 * Use uglifyjs (http://marijnhaverbeke.nl/uglifyjs)
 * to minify this script -> app.min.js
 */
$(function(){
	"use strict"

	function logResponse(response) {
		// if (console && console.log) {
		// 	console.log('The response was', response);
		// }
	}

	// Set up so we handle click on the buttons
	$('.post-to-wall').click(function(event) {
		event.preventDefault();

		var that = $(this);
		var param = { method: 'feed', link: that.data('href') };
		if ( that.data('picture') ) { param.picture = that.data('picture'); }
		if ( that.data('name') ) { param.name = that.data('name'); }
		if ( that.data('caption') ) { param.caption = that.data('caption'); }
		if ( that.data('description') ) { param.description = that.data('description'); }

		FB.ui(
			param,
			// If response is null the user canceled the dialog
			function (response) { if (response != null) { logResponse(response); } }
		);
	});

	$('.send-to-friends').click(function(event) {
		event.preventDefault();

		var that = $(this);
		var param = { method: 'send', link: that.data('href') };
		if ( that.data('name') ) { param.name = that.data('name'); }

		FB.ui(
			param,
			// If response is null the user canceled the dialog
			function (response) { if (response != null) { logResponse(response); } }
		);
	});
});