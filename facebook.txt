/**
 * ------------------------------------------------------------
 *  Notes
 * ------------------------------------------------------------
 */

/**
 * Remember to change "PAGE URL" to the page URL you want to share/like/send etc
 *
 * Remember to add the OG Tags before the </head closing head tag
 * They add meta data to your site that facebook uses when your content is shared over facebook
 * And they are used by deafult if no date is passed to "Post to wall" button.
 	<html prefix="og: http://ogp.me/ns#" lang="en">

		<!-- These are Open Graph tags.  They add meta data to your  -->
		<!-- site that facebook uses when your content is shared     -->
		<!-- over facebook.  You should fill these tags in with      -->
		<!-- your data.  To learn more about Open Graph, visit       -->
		<!-- 'https://developers.facebook.com/docs/opengraph/'       -->
		<meta prefix="fb: http://ogp.me/ns/fb#" property="fb:app_id" content="<?php echo AppInfo::appID(); ?>" />
		<meta property="og:title" content="<?php echo he($app_name); ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?php echo AppInfo::getAppUrl(); ?>" />
		<meta property="og:image" content="<?php echo AppInfo::getAppUrl('/logo.png'); ?>" />
		<meta property="og:site_name" content="<?php echo he($app_name); ?>" />
		<meta property="og:description" content="My first app" />

 * Remember to add the following code just before the </head> closing head tag
	<script type="text/javascript" src="/javascript/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="/javascript/app.min.js"></script>
 *
 * Remember to add the following code just after the <body> open body tag
	<div id="fb-root"></div>
	<script type="text/javascript">
		window.fbAsyncInit = function() {
			// Init facebook js sdk
			FB.init({
				appId      : '<?php echo AppInfo::appID(); ?>', // App ID
				channelUrl : 'https://<?php echo $_SERVER["HTTP_HOST"]; ?>/channel.html', // Channel File
				status     : true, // check login status
				cookie     : true, // enable cookies to allow the server to access the session
				xfbml      : true // parse XFBML
			});

			// set auto height
			FB.Canvas.setAutoGrow();
		};

		// Load the SDK Asynchronously
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "https://connect.facebook.net/en_US/all.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
 */



/**
 * ------------------------------------------------------------
 *  Facebook Social Plugins | Default
 * ------------------------------------------------------------
 */

/**
 *	Like
 *	------------------------------------------------------------
 *	data-href: Page to be liked
 *	data-send: Include a send button?
 *	data-width: Like container width
 *	data-show-faces: Show profile pictures bellow the button
 *  data-action: Facebook action, currently only "like" and "recommend" are supported. Default is "like"
 *	data-font: The font of the plugin
 */
<div class="fb-like" data-href="PAGE URL" data-send="false" data-width="450" data-show-faces="false" data-font="lucida grande"></div>

/**
 *	Recommend
 *	------------------------------------------------------------
 *	data-href: Page to be recommend
 *	data-send: Include a send button?
 *	data-width: Recommend container width
 *	data-show-faces: Show profile pictures bellow the button
 *  data-action: Facebook action, currently only "like" and "recommend" are supported. Default is "like"
 *	data-font: The font of the plugin
 */
<div class="fb-like" data-href="PAGE URL" data-send="false" data-width="450" data-show-faces="false" data-action="recommend" data-font="lucida grande"></div>

/**
 *	Comments
 *	------------------------------------------------------------
 *	data-href: Page to add comments to
 *	data-num-posts: The number of posts to display by default
 *  data-width: Comments container width
 */
<div class="fb-comments" data-href="PAGE URL" data-num-posts="2" data-width="470"></div>



/**
 * ------------------------------------------------------------
 *  Facebook Social Plugins | Custom
 * ------------------------------------------------------------
 */

/**
 *	Post to wall
 *	------------------------------------------------------------
 *	data-href: Page link to be posted to wall
 *  data-picture: Post picture
 *  data-name: Post link title
 *  data-caption: Post caption
 *  data-description: Post description
 *
 * No Javascript HREF
	https://www.facebook.com/dialog/feed
		?app_id=334508766624321
		&link=
		&picture=
		&name=
		&caption=
		&description=

 * E.g
 	https://www.facebook.com/dialog/feed?app_id=123050457758183&link=https://developers.facebook.com/docs/reference/dialogs/&picture=http://fbrell.com/f8.jpg&name=Facebook%20Dialogs&caption=Reference%20Documentation&%20description=Using%20Dialogs%20to%20interact%20with%20users&redirect_uri=http://www.example.com/response
 */
<a href="#post-to-wall" class="post-to-wall" data-href="PAGE URL">Post to wall</a>

/**
 *	Send to friends
 *	------------------------------------------------------------
 *	data-href: Page link to be sent to friends
 *  data-name: Post link title
 *
 * No Javascript HREF
	https://www.facebook.com/dialog/send
	?app_id=334508766624321
	&name=
	&link=

 * E.g
 	https://www.facebook.com/dialog/send?app_id=123050457758183&name=People%20Argue%20Just%20to%20Win&link=http://www.nytimes.com/2011/06/15/arts/people-argue-just-to-win-scholars-assert.html&redirect_uri=http://www.example.com/response
 */
<a href="#send-to-friends" class="send-to-friends" data-href="PAGE URL">Send to friends</a>