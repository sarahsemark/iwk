<?php

$url = "communitycare";
$title = "Community Wellness Centre";
$description = "The IWK supports the Spryfield area community through the Chebucto Community Health team and their initiatives such as parenting programs, mental health training for adults who support youth, and helping children, youth and families connect with appropriate health services and resources in their community.";


$time = "1405";

$lat = "44°36'50\"";
$long = "63°37'87\"";



$narration = <<<HEREDOC
<p>Skylights fill the waiting area of this neighbourhood clinic with a warm, welcoming glow. Framed artwork created by young hands lines the hallways. A gigantic, jovial sculpture of a fruit bowl catches the eye of everyone who enters.</p>

<p>The Community Wellness Centre is a safe place that offers a variety of health care professionals who work together to care for patients and their families. It&rsquo;s at the heart of this community. The location is the reason that Spryfield and area families, children and youth first visit, but it&rsquo;s not why they&rsquo;re willing to go back. IWK pediatrician, Dr. Tara Chobotuk, and her colleagues can take credit for that. The care they offer is attentive and compassionate. Watching seven-month-old Easton interact with Dr. Chobotuk is proof that their approach is having a positive impact. Thanks to a referral from their family doctor, supported by Dr. Chobotuk&rsquo;s accurate diagnosis and open lines of communication with the IWK&rsquo;s gastrointestinal specialists, Easton&rsquo;s acid reflux was treated quickly and effectively. Just a few weeks ago, he was crying out in discomfort or pain. Today, he&rsquo;s squealing in delight.</p>
HEREDOC;

$images = array('_DSC5619_web.jpg', '_DSC5664_web.jpg', '_DSC5703_web.jpg', '_DSC5751_web.jpg', '_DSC5789_web.jpg', '_DSC5823_web.jpg', '_DSC5890_web.jpg', '_DSC5659_web.jpg', '_DSC5689_web.jpg', '_DSC5739_web.jpg', '_DSC5756_web.jpg', '_DSC5815_web.jpg', '_DSC5852_web.jpg', '_DSC5907_web.jpg');

$quotes = array(
		'How does the IWK join a community and become part of the culture? That&rsquo;s what we&rsquo;ve set out to do. We&rsquo;re teaming up with organizations, schools and other health care providers with the common goal of keeping people in this community well. Dr. Chobotuk sees newborns to 16-year-olds, working to address everything from medical conditions and mental health issues, to behavioural challenges and growth concerns. Our goal is to develop relationships with care providers in the community. By collaborating with family doctors, family resource centres and others in the community, we can provide the best possible care.' 
	=> 'Jackie Spiers<br>Manager<br>Primary Health, IWK',
		
	'We receive the exact same care we&rsquo;d get at the IWK Health Centre, just closer to home. We feel very fortunate to have Dr. Chobotuk a 13-minute drive away, as opposed to a 43-minute drive. Some people can&rsquo;t afford to pay for parking or gas, or can&rsquo;t take the additional time off work to travel downtown. You don&rsquo;t want to have to bring your kids to the doctor, but if you do, this is a great place to come.' 
	=> 'Laura Henneberry<br>Mom to seven-month-old Easton'
);
