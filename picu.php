<?php

$url = "picu";
$title = "Pediatric Intensive Care Unit (PICU)";
$description = "The PICU is an integral part of the Cardiac Science Program providing an <em>interprofessional approach</em> to patient care. The PICU and the Children&rsquo;s Heart Centre team work <em>collaboratively</em>
to provide care to children before, during and after heart surgery.";


$time = "1930";

$lat = "44°38'15\"";
$long = "63°35'24\"";



$narration = <<<HEREDOC
<p>The technology in the Pediatric Intensive Care Unit (PICU) is so advanced it can keep organs working when they otherwise wouldn&rsquo;t, including hearts and lungs. But looking around, it&rsquo;s obvious the staff&rsquo;s expertise extends far beyond how to use complex equipment. In just one shift, the PICU team could handle anything from the intense hours following a major surgery, to the questions of a worried parent or the playfulness of a nearly-well child. In each of these situations, as well as many others, communication is the one tool that matters most.</p>
<p>The PICU is known nationally for its integrated and efficient processes in service delivery. It creates the consistency and compassion families are grateful for. Intensivists provide over-the-phone support to health care professionals as they are transporting critically ill children requiring the services and expertise of the Life Flight Program. The IWK is one of the only places in Canada that has such an integrated approach to intensive care. Communication unites children, their families and sometimes more than a dozen health care professionals to achieve a common goal: make children healthy so they can go back home.</p>
HEREDOC;

$images = array('_DSC6021_web.jpg', '_DSC6035_web.jpg', '_DSC6052_web.jpg', '_DSC6062_web.jpg', '_DSC6071_web.jpg', '_DSC6075_web.jpg', '_DSC6085_web.jpg', '_DSC6086_web.jpg');

$quotes = array(
		'We are the only hospital in Atlantic Canada that offers pediatric heart surgery. It represents about 130 patients, out of an approximate 450 who come through the PICU each year. In terms of workload, about half of what we do in the PICU involves looking after patients before or after open-heart surgery. Because it&rsquo;s so invasive, most of these children require a period of life support afterwards. That means we use advanced technology to support or replace their major organs. In the most complex situations, a machine replaces the heart and lungs at the same time. It requires a team of people with specific, advanced training. We are the only hospital in the Maritimes that cares for children needing life support.' 
	=> 'Dr. Chris Soder<br>Pediatric Intensivist, IWK',
		
	'We call all the staff by their first names. They know Katie, they know us and they make decisions as a group. It totally brings our stress level down. All we have to think about is being here for Katie.' 
	=> 'Shawn Wood<br>Dad to two-year-old Katie',
		
	'Many people ask me if it is difficult to work in a PICU. I always respond by explaining that one of the most rewarding things about working in the PICU is seeing a child go from being critically ill to smiling and getting ready to transition toward home with their family.' 
	=> 'Kim Pellerine<br>Interim Clinical Leader<br>Pediatric Intensive Care Unit, IWK'
);
