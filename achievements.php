<?php

$url = "achievements";
$title = "Board Achievements";
$description = "In 2011/2012, our community-based board of 13 volunteers from throughout the Maritimes, along with five representatives from the IWK Health Centre, continued their focus on <em>strategic direction, action and accountability</em>.";

//$time = "1405";

$lat = "44°38'14\"";
$long = "63°35'33\"";

$narration = <<<HEREDOC
<h3>The IWK Board of directors</h3>
<ul>
<li>Reconfirmed the mission, vision, values, and strategic direction of the IWK Health Centre in light of Nova Scotia&rsquo;s changing health care environment.</li>
<li>Worked collaboratively with government and other District Health Authorities to reduce the overall costs of the health care system.</li>
<li>Provided oversight to the IWK&rsquo;s involvement in the Merged Services Nova Scotia (MSNS) initiative.</li>
<li>Monitored Key Performance Indicators (KPIs), progress reports, internal and external reviews, and financial performance.</li>
<li>Monitored the impact of business plan cost reductions on morale, patient and staff satisfaction, quality, and patient safety.</li>
<li>Approved a five-year redevelopment of the IWK&rsquo;s Mental Health Inpatient Unit, NICU, PICU, pediatric rehabilitation, and the women&rsquo;s perioperative program.</li>
<li>Shared in the accomplishment of achieving &ldquo;exemplary standing&rdquo; by Accreditation Canada.</li>
</ul>
HEREDOC;
