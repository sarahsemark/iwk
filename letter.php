<?php

$url = "letter";
$title = "President&rsquo;s Message";
$description = "There are <em>24 hours</em> in a day. It doesn’t matter <em>where</em> you live, or <em>who</em> you are.";


//$time = "1405";

$lat = "44°38'23\"";
$long = "63°35'06\"";



$narration = <<<HEREDOC
<p>We like to think we make good use of every 24 hours at the Health Centre&mdash;helping, healing, changing lives and making a difference to patients and families in whatever way we can.</p>
<p>For every 24 hours at the IWK Health Centre, there are countless moments of reassurance, joy, fear, acceptance, learning, grief and discovery.</p>
<p>We want you to join us as we take you behind the scenes for a personal look at what happens at the IWK every day, as the clock ticks down the hours.</p>
<p>We are focused on continuing to provide the very best care in an environment that is not only changing constantly, but is also challenged by limited resources. This is our daily challenge&mdash;the best care provided within an environment of incredible change, new possibilities and expectations for a level of accountability for use of resources and outcomes of care never before experienced.</p>
<p>Fortunately, the IWK is already in an ideal position to fare well in this environment. We embraced the opportunity for change years before it was an absolute necessity&mdash;our Strategic Plan was written in 2007 and the key directions still ring true to us each and every day: keeping families healthy, providing the best care, using resources wisely, keeping patients safe, and being the best researchers and teachers we can be. We have bolstered our commitment to these key directions through new strategies that will sustain us and even allow us to expand our impact further into the community, to meet the expectations and needs of today’s families.</p>
<p>We think we have a lot to celebrate in what we have been able to accomplish every day, week, month and year&mdash;for the past 103 years.</p>
<p>We invite you now to take a look inside the IWK and experience a day with us.</p>

<img src="images/anne-signature.png" alt="anne-signature" width="200" height="70" />
<p class="quote_source">Anne McGuire<br>President &amp; CEO, IWK Health Centre</p>

<img src="images/rob-signature.png" alt="rob-signature" width="205" height="51" />
<p class="quote_source">Rob Richardson<br>Chair, Board of Directors, IWK Health Centre</p>

HEREDOC;
