<?php

$url = "download";
$title = "Download";
$description = "Want to <em>print</em> or <em>save</em> this report for yourself? Get it here.";


//$time = "1405";

// $lat = "44°38'23";
// $long = "63°35'06";



$narration = <<<HEREDOC
<p>Download the print version of our annual report here, in French or English.</p>
<ul>
<li><a href="/pdfs/IWK-English.pdf">English</a></li>
<li><a href="/pdfs/IWK-Francais.pdf">Francais</a></li>
</ul>

HEREDOC;
