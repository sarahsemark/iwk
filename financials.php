<?php

$url = "financials";
$title = "Financials";

$description = "Financial statements from the IWK's 2011-2012 report to the community.";


//$time = "1405";

//$lat = "44°38'14";
//$long = "63°35'33";


$narration = <<<HEREDOC

		<div class="six columns">
				
		<p>Behind the scenes of any 24-hour day at the IWK, beyond the patient&rsquo;s bedside or the emergency room full of worried parents, hardworking teams of staff, physicians and volunteers are working together to make the best use of the IWK&rsquo;s resources. This is as much a part of the culture at the IWK Health Centre as kindness, collaboration and best practices. That&rsquo;s because we are all taxpayers and donors, too. We want to maximize the money, equipment and people we have in the most efficient and effective way possible, ensuring more resources are available to invest in the care of our patients.</p>

<p>While we very much respect the financial burden already being placed on Maritime taxpayers to keep health care running, we do not believe that should be an obstacle to growth in the IWK&rsquo;s care, research and teaching mandate. To this end, we have designed our annual business planning process to be collaborative, transparent and creative and to include input from all staff and physicians, as well as patients and families.</p>

		</div>
		<div class="six columns omega">

<p>Teams at the IWK have come up with ideas that have not only saved dollars, but have also improved care at the same time. Everything from our Dial for Dining program, which cut food waste from 60 per cent down to 5 per cent almost overnight, to the day-to-day efforts of pharmacists who package bulk drugs into single-unit doses to avoid waste of very expensive drugs&mdash;a system which is also safer for patients. These are just two examples; the list goes on.</p>
<p>We see it as our responsibility to spend every dollar we have wisely. And, we are proud of everyone who helps support these efforts, 24 hours a day, every day.</p>


<img src="images/allan-signature.png" alt="allan-signature" width="200" height="85" />
<p class="quote_source">Allan Horsburgh, CA<br>
Vice-President &amp; Chief Financial Officer<br> 
IWK Health Centre</p>


		</div>
		
		<div class="twelve columns">

<div class="form-header">
<h2>Balance sheet</h2>
<h3>March 31, 2012</h3>
</div>

<table>
	<thead>
		<tr>
			<th>Assets</th>
			<th>2012</th>
			<th>2011</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>Cash and cash equivalents</td>
			<td>21,567,000</td>
			<td>21,591,000</td>
		</tr>
	</tbody>
	
	
	<tbody>
		<tr>
			<td>Receivables and prepaids</td>
			<td>65,443,000</td>
			<td>52,201,000</td>
		</tr>
	</tbody>
	
	<tbody>
		<tr>
			<td>Inventories</td>
			<td>1,483,000</td>
			<td>1,969,000</td>
		</tr>
	</tbody>
	
	<tbody>
		<tr>
			<td>Property and equipment</td>
			<td>171,999,000</td>
			<td>179,742,000</td>
		</tr>
	</tbody>
	
	<tfoot>
		<tr>
			<td></td>
			<td>260,492,000</td>
			<td>255,503,000</td>
		</tr>
	</tfoot>		
</table>



<table>
	<thead>
		<tr>
			<th>Liabilities</th>
			<th>2012</th>
			<th>2011</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>Payables and accruals</td>
			<td>40,733,000</td>
			<td>38,886,000</td>
		</tr>
	</tbody>
	
	<tbody>
		<tr>
			<td>Retirement allowances and benefits</td>
			<td>28,490,000</td>
			<td>26,230,000</td>
		</tr>
	</tbody>
	
	<tbody>
		<tr>
			<td>Facilities loan payable</td>
			<td>11,489,000</td>
			<td>12,156,000</td>
		</tr>
	</tbody>
	
	<tbody>
		<tr>
			<td>Appropriations and reserves</td>
			<td>19,266,000</td>
			<td>10,645,000</td>
		</tr>
	</tbody>
	
	<tfoot class="subtotal">
		<tr>
			<td></td>
			<td>99,978,000</td>
			<td>87,917,000</td>
		</tr>
	</tfoot>	
</table>



<table>
	<thead>
		<tr>
			<th>Equity</th>
			<th>2012</th>
			<th>2011</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td></td>
			<td>160,514,000</td>
			<td>167,586,000</td>
		</tr>
	</tbody>
	
	<tfoot>
		<tr>
			<td></td>
			<td>260,492,000</td>
			<td>255,503,000</td>
		</tr>
	</tfoot>	
</table>

<div class="form-header">
<h2>Statement of operations</h2>
<h3>Year ended March 31, 2012</h2>
</div>




<table>
	<thead>
		<tr>
			<th>Revenue</th>
			<th>2012</th>
			<th>2011</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>Retail revenue</td>
			<td>7,924,000</td>
			<td>7,675,000</td>
		</tr>
	</tbody>
	
	
	<tbody>
		<tr>
			<td>Taxpayers</td>
			<td>222,966,000</td>
			<td>217,737,000</td>
		</tr>
	</tbody>
	
	<tbody>
		<tr>
			<td>IWK Foundation*</td>
			<td>250,000</td>
			<td>250,000</td>
		</tr>
	</tbody>
	
	<tbody>
		<tr>
			<td>Out-of-province and out-of-country</td>
			<td>8,826,000</td>
			<td>9,145,000</td>
		</tr>
	</tbody>
	
	<tfoot class="subtotal">
		<tr>
			<td></td>
			<td>239,966,000</td>
			<td>234,807,000</td>
		</tr>
	</tfoot>		
</table>

<p class="small">*The IWK Foundation also provides $3.75 million to the IWK to purchase priority equipment, and fund research and fellowships.</p>



<table>
	<thead>
		<tr>
			<th>Expenses</th>
			<th>2012</th>
			<th>2011</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>Administration</td>
			<td>3,823,000</td>
			<td>3,788,000</td>
		</tr>
	</tbody>
	
	<tbody>
		<tr>
			<td>Operations &amp; Support Services <span class="small">(Utilities, Information Technology, Maintenance, Housekeeping, Food Services, etc.)</span></td>
			<td>59,049,000</td>
			<td>53,380,000</td>
		</tr>
	</tbody>
	
	<tbody>
		<tr>
			<td>Medical Services <span class="small">(Pathology, Lab, Diagnostic Imaging, physician administration, etc.)</span></td>
			<td>24,778,000</td>
			<td>24,801,000</td>
		</tr>
	</tbody>
	
	<tbody>
		<tr>
			<td>Women&rsquo;s &amp; Newborn Health <span class="small">(Neonatal Intensive Care Unit (NICU), Birth Unit, Operating Rooms (ORs), Breast Health, etc.)</span></td>
			<td>46,337,000</td>
			<td>46,946,000</td>
		</tr>
	</tbody>

	<tbody>
		<tr>
			<td>Mental Health &amp; Addictions <span class="small">(CHOICES, Adolescent Centre for Treatment, Compass, etc.)</span></td>
			<td>26,837,000</td>
			<td>26,177,000</td>
		</tr>
	</tbody>
	
	<tbody>
		<tr>
			<td>Children&rsquo;s Health <span class="small">(Pediatric Intensive Care Unit, ORs, Inpatient Units, Ambulatory Care, etc.)</span></td>
			<td>59,664,000</td>
			<td>59,480,000</td>
		</tr>
	</tbody>
	
	
	<tbody>
		<tr>
			<td>Provincial Programs <span class="small">(HITS-NS)</span></td>
			<td>18,830,000</td>
			<td>17,740,000</td>
		</tr>
	</tbody>	

	
	<tbody>
		<tr>
			<td>Capital investment</td>
			<td>648,000</td>
			<td>2,495,000</td>
		</tr>
	</tbody>

		
	<tfoot class="subtotal">
		<tr>
			<td>Total Expenses</td>
			<td>239,966,000</td>
			<td>234,807,000</td>
		</tr>
	</tfoot>	
</table>


<table>
	<tfoot>
		<tr>
			<td>Net Surplus (loss) from Operations</td>
			<td>0</td>
			<td>0</td>
		</tr>
	</tfoot>	
</table>		
<p class="small"><em>A year end transfer from operations to capital resulted in a balanced operating position in each of the last two fiscal years. Statutory financial statements, as reported on by the IWK Health Centre&rsquo;s auditors, Grant Thornton, are available in the office of the Vice President &amp; Chief Financial Officer.</em></p>
		</div>

HEREDOC;
