<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes" />

    <title>Capital Health Localtest</title>



	<!-- CSS
  ================================================== -->
	<link rel="stylesheet" href="skeleton/stylesheets/base.css">
	<link rel="stylesheet" href="skeleton/stylesheets/skeleton.css">
	<link rel="stylesheet" href="skeleton/stylesheets/layout.css">
	
	<link rel="stylesheet" href="fonts/digital/stylesheet.css">
	<link rel="stylesheet" href="fonts/frutiger/stylesheet.css">
	<link rel="stylesheet" href="fonts/archer/stylesheet.css">
	<link rel="stylesheet" href="fonts/museo-slab-light/stylesheet.css">	
	
	

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	
	<script src="lightbox/js/lightbox.js"></script>
	<link href="lightbox/css/lightbox.css" rel="stylesheet" />

  </head>
  <body id="breastclinic">



	<!-- Primary Page Layout
	================================================== -->

	<!-- Delete everything in this .container and get started on your own site! -->

	<div class="container">
		<div class="twelve columns">
			<h1>Breast Health Clinic</h1>
			<hr />
		</div>
		
		<div class="twelve columns factoid">
		Of the 500 breast cancer surgeries performed at the IWK each year, approximately 1% of them are done on men. Consolidation of breast surgery at the IWK has resulted in significant decreases in wait times for both women and men.
		</div>
		
		
		<div class="eight columns clearfix">
		
		<a href="images/breast-health/_DSC6140_web.jpg" rel="lightbox[breast]"><img src="images/breast-clinic.jpg" alt="Breast Clinic" width="457" height="457"></a>
		<a id="viewmore" href="images/breast-health/_DSC6131_web.jpg" rel="lightbox[breast]">View more images</a>
		
		<div class="extra-images">
			<a href="images/breast-health/_DSC6115_web.jpg" rel="lightbox[breast]" title="Optional caption.">Image</a>
			<a href="images/breast-health/_DSC6098_web.jpg" rel="lightbox[breast]" title="Optional caption.">Image</a>
			<a href="images/breast-health/_DSC6129_web.jpg" rel="lightbox[breast]" title="Optional caption.">Image</a>
		</div>
				
		<blockquote><span class="bqstart">&ldquo;</span>Women have such pivotal roles in households and communities; their illness places a significant stress on the entire family unit. How we support those families has to be very fluid and involving them in care helps us understand what they need. We absolutely can&rsquo;t leave them out when caring for a woman with breast cancer.</blockquote>
<p class="quote_source">Joanne Robar<br>Project Lead Breast Health Services<br>IWK 4 South</p>

		<blockquote><span class="bqstart">&ldquo;</span>It&rsquo;s important that the kids here know this is a place for and about them. They are extremely resilient and they do get better. We&rsquo;re really fortunate to work with them.</blockquote>
<p class="quote_source">Melanie Goodday<br>Teacher<br>Therapeutic Classroom 4 South, IWK</p>

		<blockquote><span class="bqstart">&ldquo;</span>At first, being here is a bit scary because you don&rsquo;t know anyone. But the staff are all really nice and the other kids are pretty cool, too. We all get along really well.</blockquote>
<p class="quote_source">Jessica<br>IWK patient</p>


		</div>
		
		
		
		
		
		<div class="eight columns">
		
		<div class="four columns offset-by-four time">0905</div>

		<div class="four columns alpha"> &nbsp; </div>
		
		<div class="four columns omega coords"><p>41°57.161&rsquo; N</p><p>71°27.128&rsquo; W</p></div>
		
		
		<div class="narration">
		
		<p>This isn&rsquo;t like the harshly lit, hectic operating rooms depicted on television. The five-person team works efficiently and quietly. When they do speak, it&rsquo;s in shorthand, referring to instruments or actions by abbreviated medical terms that a layperson wouldn&rsquo;t understand. There are supplies, machines and cords everywhere, but the room is anything but in disarray. It is calm; the movements calculated and controlled. This is the team&rsquo;s first of four breast surgeries today, which is typical in a clinic that completes approximately 500 surgeries a year. It&rsquo;s been about four years since breast surgery, including lumpectomies and mastectomies, have moved from the QEII Health Sciences Centre and Dartmouth General to the IWK. It was an important step in a multi-phase journey to build a world-class, comprehensive breast health centre. There, women, men and their families will access all aspects of breast health care, from diagnosis right through to recovery and survivorship. But in the meantime, surgery wait times have been reduced and the IWK&rsquo;s family-inclusive approach to care is having a positive impact on the patient experience and outcomes. </p>
		
		<p>Outside the operating room and down a series of hallways, the patient&rsquo;s husband, two daughters and son-in-law await the news that surgery went well. They occupy almost every chair in a small waiting room, but that doesn&rsquo;t matter. The IWK is grounded in principles and philosophies of family-centred care and believe it is absolutely critical in women&rsquo;s health. By embracing a woman&rsquo;s support system, the care team can better understand the impacts of illness on her family and can provide guidance, support and information that enables women to go home – where the healing process is much faster. </p>
		
		</div>
		
		

		</div>
		
	</div><!-- container -->



	<sidebar>
	<ul>
		<li><a id="presidents-link" href="#">President&rsquo;s Message</a></li>
		<li><a id="breastclinic-link" href="#">Breast Health Clinic</a></li>
		<li><a id="mentalhealth-link" href="#">Mental Health Inpatient Services</a></li>
		<li><a id="communitycare-link" href="#">Community Care Team</a></li>
		<li><a id="nicu-link" href="#">Neonatal Intensive Care Unit (NICU)</a></li>
		<li><a id="picu-link" href="#">Pediatric Intensive Care Unit (PICU)</a></li>
		<li><a id="pediatric-link" href="#">Pediatric Rehabilitation Care Team</a></li>
		<li><a id="emergency-link" href="#">Emergency Department</a></li>
		<li><a id="facts-link" href="#">Facts, Stats, and Numbers</a></li>
		<li><a id="charts-link" href="#">Charts &amp; Graphs</a></li>
		<li><a id="pdf-link" href="english.pdf">Download PDFs</a></li>
	</ul>
	</sidebar>



  </body>
</html>