<?php

$url = "breastclinic";
$title = "Breast Health Clinic";
$description = "Of the <em>500</em> breast cancer surgeries performed at the IWK each year, approximately 1% of them are on men. Consolidation of breast surgery at the IWK has resulted in significant <em>decreases in wait times</em> for both women and men.";


$time = "0905";

$lat = "44°38'26\"";
$long = "63°35'05\"";



$narration = <<<HEREDOC
<p>This isn&rsquo;t like the harshly lit, hectic operating rooms depicted on television. The five-person team works efficiently and quietly. When they do speak, it&rsquo;s in shorthand, referring to instruments or actions by abbreviated medical terms that a layperson wouldn&rsquo;t understand. There are supplies, machines and cords everywhere, but the room is anything but in disarray. It is calm; the movements calculated and controlled. This is the team&rsquo;s first of four breast surgeries today, which is typical in a clinic that completes approximately 500 surgeries a year. It&rsquo;s been about four years since breast surgery, including lumpectomies and mastectomies, moved from the QEII Health Sciences Centre and Dartmouth General to the IWK. It was an important step in a multi-phase journey to build a world-class, comprehensive Breast Health Centre. There, women, men and their families will access all aspects of breast health care, from diagnosis right through to recovery and survivorship. But in the meantime, surgery wait times have been reduced and the IWK&rsquo;s family-inclusive approach to care is having a positive impact on the patient experience and outcomes.</p>

<p>Outside the operating room and down a series of hallways, the patient&rsquo;s husband, two daughters and son-in-law await the news that surgery went well. They occupy almost every chair in a small waiting room, but that doesn&rsquo;t matter. The IWK is grounded in principles and philosophies of family-centred care and believe it is absolutely critical in women&rsquo;s health. By embracing a woman&rsquo;s support system, the care team can better understand the impacts of illness on her family and can provide guidance, support and information that enable women to go home&mdash;where the healing process is much faster.</p>
HEREDOC;

$images = array('_DSC6138_web.jpg', '_DSC6152_web.jpg', '_DSC6173_web.jpg', '_DSC6194_web.jpg', '_DSC6205_web.jpg', '_DSC6209_web.jpg', '_DSC6115_web.jpg', '_DSC6131_web.jpg', '_DSC6140_web.jpg', '_DSC6170_web.jpg', '_DSC6176_web.jpg', '_DSC6199_web.jpg', '_DSC6206_web.jpg', '_DSC6213_web.jpg', 'comp.jpg');

$quotes = array(
	'Women have such pivotal roles in households and communities; their illness places a significant stress on the entire family unit. How we support those families has to be very fluid and involving them in care helps us understand what they need. We absolutely can&rsquo;t leave them out when caring for a woman with breast cancer.' 
	=> 'Joanne Robar<br>Project Lead<br>Breast Health Services<br>IWK',
	
	'It&rsquo;s very important for families to be together. Waiting is the hard part and we&rsquo;re so grateful to be able to do that together.' 
	=> 'Shirley MacPherson<br>Daughter of Breast Health Clinic patient, Aly Reinders<br>From the pre-op waiting room'
	
);
