<?php

$url = "nicu";
$title = "Neonatal Intensive Care Unit (NICU)";
$description = "In 2011/2012, <em>963 infants</em> were admitted to the NICU. Of those, <em>797</em> were born at the IWK, <em>30</em> were from New Brunswick, <em>36</em> from Prince Edward Island, seven from Newfoundland, and eight from Bermuda.";


$time = "1745";

$lat = "44°38'14\"";
$long = "63°35'31\"";



$narration = <<<HEREDOC
<p>Her name is Madelyn. She arrived via Life Flight from Sydney, Cape Breton after being born at 24 weeks. That was 63 days ago. Her mom, Meaghan, spends the majority of her time in the chair beside her incubator keeping a watchful eye over her tiny girl. Despite the staff and equipment that fill the unit&mdash;and there are a lot of both&mdash;loved ones are always welcome. The hours that Madelyn spends against her mother&rsquo;s bare chest are vital to her care. Skin-to-skin contact, otherwise known as Kangaroo Mother Care, is a wonderful way for a mother and baby to get to know one another and enhance their relationship.</p>
<p>Skin-to-skin contact helps transition infants from uterine life to being out in the world. It helps to stabilize their heart rate, respirations and glucose, to establish breastfeeding and even to alleviate pain. Madelyn sleeps contently there in skin-to-skin contact in the midst of the busy intensive care unit, thriving on her mother&rsquo;s care.</p>
<p>On the other side of the unit, staff are preparing themselves for situations that are not as idyllic. When a baby &ldquo;crashes&rdquo; they must respond instantly with a full court press of clinical staff and equipment&mdash;a &rdquo;code blue.&rdquo; This team must be razor sharp with their clinical skills and communication, so they use simulated patient situations to prepare. State-of-the-art mannequins and monitoring machines make it possible for them to practice like it is the “real thing”&mdash;ensuring that tiny infants like Madelyn will always have a fighting chance.<p>
<p>By combining advanced equipment, exceptional teamwork and the intimate support that only families can provide, it&rsquo;s clear the NICU is a special place, providing very special care.</p>
HEREDOC;

$images = array('_DSC5928_web.jpg', '_DSC5943_web.jpg', '_DSC5967_web.jpg', '_DSC5976_web.jpg', '_DSC5983_web.jpg', '_DSC5991_web.jpg', '_DSC6005_web.jpg', '_DSC5938_web.jpg', '_DSC5945_web.jpg', '_DSC5972_web.jpg', '_DSC5981_web.jpg', '_DSC5987_web.jpg', '_DSC5997_web.jpg', '_DSC6012_web.jpg');

$quotes = array(
		'On any given shift we have 35 staff and they&rsquo;re all vital. There are nurses, respiratory therapists, physicians, ward clerks, unit aides, housekeepers; the list goes on. Each job is as important as the next. For instance, one night I had to perform a resuscitation but the baby was in a crib. That&rsquo;s not the best space for me to work in this particular situation. The unit aide was able to go get what we call a cozy cot and get it set up - and fast! I was able to lift the baby up and she moved the crib, whipped the cozy cot in, and just like that we had access to the baby from three sides. She did that without me having to ask her. That sort of teamwork is an integral part of what we do here.' 
	=> 'Catherine Denman<br>Registered Nurse, IWK',
		
	'The staff here are really great. They become part of your family for a little while. It&rsquo;s amazing how many babies come in and out of the unit. I never realized how many are born premature.' 
	=> 'Meaghan Bragg<br>Mom to two-month-old Madelyn'
);
