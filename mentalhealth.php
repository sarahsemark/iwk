<?php

$url = "mentalhealth";
$title = "Mental Health Inpatient Services";
$description = "4 South&rsquo;s <em>multi-provider approach</em> to care ensures that patients and families receive the services that best meet their needs.";


$time = "1115";

$lat = "44°38'16\"";
$long = "63°34'57\"";



$narration = <<<HEREDOC
<p>In many ways, the wing of the IWK known as 4 South looks like any other place where children or teenagers live. The beds are unmade. A purple quilt is scrunched up near the foot of one, several pillows piled at the head of another. There are paintings and drawings tacked to the wall and a basketball hoop and potted plants outside. The children and youth who live here are like all patients at the IWK: they just want to get well. For young people like Jessica, making new friends, exploring creativity and tackling interesting projects are an important part of doing that.</p>
<p>That&rsquo;s why the patients here are immersed in structured programming that is designed to meet their specific needs. Attending the Tall Ships Festival Parade of Sail, tending to an urban garden plot every week, and creating and producing a monthly newspaper are examples of activities patients engage in. Jessica is one of the contributing columnists to the newspaper. A top student in English class, she&rsquo;s a natural and shared stories about a family vacation in Europe. Trying her hand at publishing, she coordinated the topics, articles, design, and layout for one issue, as well. In this unit, like every other, the whole patient is cared for, not just their illness or injury.</p>
<p>Medicine is important, but it doesn&rsquo;t tell the whole story. This is about living during the healing.</p>
HEREDOC;

$images = array('_DSC6220_web.jpg', '_DSC6229_web.jpg', '_DSC6253_web.jpg', '_DSC6274_web.jpg', '_DSC6280_web.jpg', '_DSC6291_web.jpg', '_DSC6225_web.jpg');

$quotes = array(
		'It&rsquo;s important that the kids here know this is a place for and about them. They are extremely resilient and they do get better. We&rsquo;re really fortunate to work with them.' 
	=> 'Melanie Goodday<br>Teacher<br>Therapeutic Classroom, 4 South, IWK',
		
	'At first, being here is a bit scary because you don&rsquo;t know anyone. But the staff are all really nice and the other kids are pretty cool, too. We all get along really well.' 
	=> 'Jessica<br>IWK Patient'
);
