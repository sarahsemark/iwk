<?php

$url = "emergency";
$title = "Emergency Department";
$description = "<em>2,475 patients</em> treated by the IWK Emergency Department in 2010/2011 participated in research studies.";


$time = "1550";

$lat = "44°38'13\"";
$long = "63°35'04\"";



$narration = <<<HEREDOC
<p>Emergency rooms are unpredictable. Anything from a life-threatening injury to a fast-moving virus can come through the sliding glass doors. Without warning, any time of day or night. The health professionals here think and act quickly&mdash;treating as many as 28,000 patients a year.</p>
<p>Staff are highly trained to perform under intense stress in emergency situations. But the deeper level of understanding that comes from research&mdash;and analyzing processes, people and patients&mdash;plays an important role in making the IWK emergency room tick. Emergency care providers rely on concrete information from research to provide the best care for patients.</p>
<p>On any given day, 25 different research projects are in varying stages of development, execution or completion. The focus of each study is unique, but together, they are catalysts for continuously improving the inner workings of an emergency department&mdash;not just at the IWK but at hospitals across the country. The IWK is part of a pediatric research network that connects approximately 100 emergency researchers from across Canada. Many of these researchers are in other pediatric health centres, but recently the IWK has also been working with counterparts in smaller communities in the province.</p>
<p>By contributing to each other&rsquo;s studies, they have access to larger sample sizes and, as a result, can influence change in emergency rooms across the province, ensuring children receive the best care no matter where they live. Research can shed light on a wide range of topics; how to most effectively communicate discharge instructions and how to best treat minor head injuries are just two that the team are exploring. No matter what the topic, their goal is always the same: continuously raise the standard of emergency care for children and youth.</p>
HEREDOC;

$images = array('_DSC6385_web.jpg', '_DSC6388_web.jpg', '_DSC6396_web.jpg', '_DSC6411_web.jpg', '_DSC6387_web.jpg', '_DSC6392_web.jpg', '_DSC6399_web.jpg', '_DSC6430_web.jpg');

$quotes = array(
		'Research on emergency care for children and youth is a relatively new area, and there is great enthusiasm and interest in it. Because 80 to 90 per cent of children and youth who need emergency care across Canada are actually seen in non-pediatric emergency rooms, the IWK&rsquo;s research can contribute to the body of knowledge about the best care for these young patients and improve the overall standard of care for children at general hospitals throughout the Maritimes and across the country. The research we do here will help to improve the care they get. Our research program is creating the opportunity to answer questions that clinicians have had for a long time. It links research and practice together.' 
	=> 'Eleanor Fitzpatrick<br>Coordinator, Research Program<br>Emergency Department, IWK'
);
